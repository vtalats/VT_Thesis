# Description
This is a Latex class was created for writing a thesis or dissertation at Virginia Tech.  To get started you should 
* Download a zip file of this repository (button in the upper right of this page).
* Unzip the files into the working directory for your thesis/dissertation.
* Add `VTthesis.cls` to your path or keep it in the same directory as your LaTeX file.
* Copy `VTthesis_template.tex` to your working LaTeX file, e.g. `aml_thesis.tex`.
* Update your personal information (e.g. name, program, degree, etc.) in your LaTeX file.
* Add a bib file for your references and figures in the figures directory.

# Detailed Documentation

## Preamble

### Document Class

In addition to the standard document class formatting options for a report, the following options are defined for the VTthesis class: 
* **proposal**
* **prelim**
* **doublespace**
* **draft**

The default document is 12pt, two-sided, letter paper.  The document defined below would be 10pt, two-sided, double spaced, letter paper.

```TeX
\documentclass[10pt,doublespace]{VTthesis}
```

Using the following header instead will create a draft copy of your thesis

```TeX
\documentclass[doublespace,draft]{VTthesis}
```

You can define the personal information for your thesis in the preamble using the following  

### Title and author of the thesis

Your name should include your middle initial.

```TeX
\title{Title of your thesis goes here}
\author{Alan M. Lattimer}
```


### Keywords 

You should include 3-5 keywords, separated by commas to be used to search for your thesis.

```TeX
\keywords{Some Keywords, Subject matter, etc.}
```

### Degree information

Change this to your program, e.g. Physics, Civil Engineering, etc.

```TeX
\program{Mathematics} 
```

Change this to your degree, e.g. Masters of Science, Masters of Art, etc.

```TeX
\degree{Doctor of Philosophy} 
```

Currently the class defaults to Virginia Tech as the degree granting institution.  To change this, uncomment the lines below in your LaTeX file and fill in your institution and city/state of the school.

```TeX
\institution{Harvard University}
\instaddress{Cambridge, MA}
```


% The submit date is typically your defense date, however, this may vary depending on the institution.

```TeX 
\submitdate{March 23, 2016} 
```

### Committee

There are slots to have five readers, one chair and one co-chair.  Only use the ones you need and comment out the ones you don't need.  If you declare a co-chair, the principal and co-advisors will be listed as co-advisors on the title page.  If you have more than seven committee members, good luck.  Per the VT ETD standards, you should not include titles or educational qualifications such as PhD or Dr.  You should however include middle initials if possible.

```TeX
\principaladvisor{John Q. Williams}
\coadvisor{Vicente Esparza}
\firstreader{Janet K. Martin}
\secondreader{Kara M. Jones}
\thirdreader{James Smith}
%\fourthreader{Fourth Committee Member}
%\fifthreader{Fifth Committee Member}
```

### Dedication and Acknowledgments

The dedication and acknowledgment pages are optional.  Uncomment to include them in your thesis or dissertation

```TeX
\dedication{This is where you put your dedications.}
\acknowledge{This is where you put your acknowledgments.}
```

### Abstracts

The abstract is required, and at VT, it should be <=250 words for thesis, <=350 words for dissertation.  For other institutions, you should consult the graduate school or department policies. 

```TeX
\abstract{Give a brief description of your thesis here. Max of 250 words for a master's thesis and 350 words for a PhD dissertation according to the VT ETD standards.}
```

The general audience abstract is now required at Virginia Tech. There are currently no word limits, but adhering to the same standards as the abstract is probably a good practice.  This is likely to not be required by other institutions and, in that case, can just be commented out or removed. 

```TeX
\abstractgenaud{At VT, you are also required as of Spring 2016 to include a general audience abstract.  This should be geared towards individuals outside of your field that may be reading seeking information about your work.  You should avoid language that is particular to your field and clearly define any terms that may have special meaning in your discipline.}
```

## Main Document

### Document Front Matter

The following lines set up the front matter of your thesis or dissertation and is required to ensure proper formatting per the VT ETD standards. 

```TeX
\frontmatter
\maketitle
\tableofcontents
```

The list of figures and tables are now optional per the official ETD standards.  Unless you have a very good reason for removing them, you should leave these lists in the document.  Comment them out to remove them.

```TeX
\listoffigures
\listoftables
```

The following sets up the document for the main part of the thesis or dissertation. Do not comment out or remove this line.

```Tex
\mainmatter
```

### Main Part of Document
At this point, you can begin writing your actual thesis or dissertation.  The chapter is the top level sectioning. An example of how that might be laid out is given below.  Note that this is a suggestion and your chapters/sections may vary based on graduate school or department guidelines and input from your committee.  It is a good practice to always label your chapters and sections so that you can refer to them later if needed.

```TeX
\chapter{Introduction} \label{ch:introduction}
  \section{One Section} \label{se:one_section}
    \subsection{A sub-section} \label{ss:this_subsection}
  \section{Another Section} \label{se:another_section}
\chapter{Review of Literature} \label{ch:lit_review}
\chapter{Results} \label{ch:results}
\chapter{Discussion} \label{ch:discussion}
\chapter{Conclusions} \label{ch:conclusions}
\chapter{Summary} \label{ch:summary}
```

### Bibliography

To include your references, they should be stored in a standard BibTeX file in the same directory as your LaTeX file. There are many ways to maintain your references and most reference management software packages will output in a standard BibTeX file.  For a list of some common (and not so common) packages, there is a comparison at <https://en.wikipedia.org/wiki/Comparison_of_reference_management_software>. When you refer to the BibTeX file here, do not include the .bib extension in <bib_file_name>.  In order to add the references, uncomment the following lines in your LaTeX file.

```TeX
\bibliography{<bib_file_name>}
\bibliographystyle{plainnat}   
```

### Appendices

The following line is required to format the chapter name to **Appendix** and to properly define the headers

```TeX
\appendix
```

Next, add your appendices.  You must leave the appendices enclosed in the appendices environment as shown in the code snippet below for the table of contents to display correctly.

```TeX
\begin{appendices}
  \chapter{First Appendix} \label{app:appendix_one}
    \section{Section one} \label{ase:app_one_sect_1}
      \lipsum[1-3]
    \section{Section two} \label{ase:app_one_sect_2}
      \lipsum[1-3]
  \chapter{Second Appendix} \label{app:appendix_two}
    \lipsum[2]
\end{appendices}
```

# Comments and Suggestions 

Below are some general suggestions for writing you dissertation

+ Label everything with a meaningful prefix so that you can refer back to sections, tables, figures, equations, etc.  Usage `\label{<prefix>:<label_name>}` where some suggested prefixes are:

| Prefix   | Environment      |
|----------|------------------|
| **ch**   | Chapter          |
| **se**   | Section          |
| **ss**   | Subsection       |
| **sss**  | Sub-subsection   |
| **app**  | Appendix         |
| **ase**  | Appendix section |
| **tab**  | Tables           |
| **fig**  | Figures          |
| **sfig** | Sub-figures      |
| **eq**   | Equations        |

+ The VTthesis class provides for natbib citations.  Your references should be in one or more `*.bib` BibTeX files saved in your working directory. Suppose you have two bib files: some_refs.bib and other_refs.bib.  Then your bibliography line to include them will be:

  ```TeX
  \bibliography{some_refs,other_refs}
  ```
  
  where multiple files are separated by commas.  In the body of 
  your work you can cite your references using natbib citations.
  Examples:

| Citation                      | Output                     |
|-------------------------------|:--------------------------:|
| `\cite{doe_title_2016}`       | [18]                       |
| `\citet{doe_title_2016}`      | Doe et al. [18]            |
| `\citet*{doe_title_2016}`     | Doe, Jones, and Smith [18] |

  For a complete list of options see <https://www.ctan.org/pkg/natbib?lang=en>.

+ Here is a sample table.  Notice that caption is centered at the top. Also notice that we use **booktabs** formatting.  You should not use vertical lines in your tables.

  ```TeX
  \begin{table}[htb]
    \centering
    \caption{Approximate computation times in hh:mm:ss for full order versus reduced order models.}
    \begin{tabular}{ccc}
      \toprule
      & \multicolumn{2}{c}{Computation Time}\\
      \cmidrule(r){2-3}
      $\overline{U}_{in}$ m/s & Full Model & ROM \\
      \midrule
      0.90 & 2:00:00 & 2:08:00\\
      0.88 & 2:00:00 & 0:00:03\\
      0.92 & 2:00:00 & 0:00:03\\
      \midrule
      Total & 6:00:00 & 2:08:06\\
      \bottomrule
    \end{tabular}
    \label{tab:time_rom}
  \end{table}
  ```

+ Below are some sample figures.  Notice the caption is centered below the figure.
  * Single centered figure:

    ```TeX
    \begin{figure}[htb]
      \centering
      \includegraphics[scale=0.5]{my_figure.eps}
      \caption{Average outlet velocity magnitude given an average input velocity magnitude of 0.88 m/s.} 
      \label{fig:output_rom}
    \end{figure}
    ```

  * Two by two grid of figures with subcaptions

    ```TeX
    \begin{figure}[htb]
      \centering
      \begin{subfigure}[h]{0.45\textwidth}
        \centering
        \includegraphics[scale=0.4]{figure_1_1.eps}
        \caption{Subcpation number one}
        \label{sfig:first_subfig}
      \end{subfigure}
      \begin{subfigure}[h]{0.45\textwidth}
        \centering
        \includegraphics[scale=0.4]{figure_1_2.png}
        \caption{Subcpation number two}
        \label{sfig:second_subfig}
      \end{subfigure}
    
      \begin{subfigure}[h]{0.45\textwidth}
        \centering
        \includegraphics[scale=0.4]{figure_2_1.pdf}
        \caption{Subcpation number three}
        \label{sfig:third_subfig}
      \end{subfigure}
      \begin{subfigure}[h]{0.45\textwidth}
        \centering
        \includegraphics[scale=0.4]{figure_2_2.eps}
        \caption{Subcpation number four}
        \label{sfig:fourth_subfig}
      \end{subfigure}
      \caption{Here is my main caption describing the relationship between the 4 subimages}
      \label{fig:main_figure}
    \end{figure}
    ```

+ You should place your images and figures in a subdirectory.  You can then tell LaTeX to look for all your images there using:

  ```TeX
  \graphicspath{{figures/}}
  ```

Note: you must have both sets of curly braces and include the final directory /

# Included Packages and Definitions

The following is a list of definitions and packages provided by VTthesis:

* The following packages are provided by the VTthesis class:
  - amsmath
  - amsthm
  - amssymb
  - enumerate
  - natbib
  - hyperref
  - graphicx
  - tikz (with shapes and arrows libraries)
  - caption
  - subcaption
  - listings
  - verbatim

* The following theorem environments are defined by VTthesis:
  - theorem
  - proposition
  - lemma
  - corollary
  - conjecture

* The following definition environments are defined by VTthesis:
  - definition
  - example
  - remark
  - algorithm

---------------------------------------------------------------------------

_I hope this template file and the VTthesis class will keep you from having to worry about the formatting and allow to focus on the actual writing.  Good luck and happy writing._

**Alan Lattimer, VT, 2016**

